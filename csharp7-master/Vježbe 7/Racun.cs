﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vježbe_7 {
    class Racun<T> : IRacun {
        public Racun(T iznosRacuna) {
            IznosRacuna = iznosRacuna;
            DatumIzdavanja = DateTime.UtcNow;
        }

        public decimal DohvatiIznos() {
            return Convert.ToDecimal(IznosRacuna);
        }

        public DateTime DohvatiDatumIzdavanja() {
            return DatumIzdavanja;
        }

        public T IznosRacuna {
            get {
                return iznosRacuna;
            }
            set {
                if (Convert.ToDouble(value) < 10) {
                    throw new IznimkaBlagajne("Manje od 10");
                }
                else {
                    iznosRacuna = value;
                }
            }
        }

        // auto property
        public DateTime DatumIzdavanja { get; set; }

        private T iznosRacuna;
    }
}
