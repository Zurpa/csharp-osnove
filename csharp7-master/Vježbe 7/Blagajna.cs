﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vježbe_7 {
    class Blagajna {
        public Blagajna() {
            lista = new List<IRacun>();
        }

        public void DodajRacun(IRacun racun) {
            lista.Add(racun);
        }

        public void PrikaziRacune() {
            foreach (IRacun racun in lista) {
                Console.WriteLine(racun.DohvatiIznos());
                Console.WriteLine(racun.DohvatiDatumIzdavanja()); 
            }
        }

        private List<IRacun> lista;
    }
}
