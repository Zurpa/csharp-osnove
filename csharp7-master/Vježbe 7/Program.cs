﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vježbe_7 {
    class Program {
        static void Main(string[] args) {
            Racun<double> r1 = new Racun<double>(25);
            Racun<double> r2 = new Racun<double>(2345);
            
            Blagajna b1 = new Blagajna();

            b1.DodajRacun(r1);
            b1.DodajRacun(r2);

            b1.PrikaziRacune();

            try {
                Racun<double> r3 = new Racun<double>(5);
            } catch (IznimkaBlagajne i) {
                Console.WriteLine("Neuspjelo dodavanje racuna zbog: {0}", i.Message);
            }
        }
    }
}
